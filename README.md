# Live Display

Just a page with a prompt that displays in fullscreen (full browser viewport) what you type. Might be helpful for announcements or giving a talk without slides.

For the next _slide_ or command you would hit command-a (or ctrl-a) and proceed typing.


## Commands

```/center```
will make all text displayed centered. The command is immediately replaced with the currently displayed text.

```/left```
will make all text displayed left aligned. The command is immediately replaced with the currently displayed text.

```/img {param}```
will display the image from the source given as param.

```/site {param}```
will display the webpage from the source given as param in an iframe. Probably not the best idea and will be either reworked or deprecated.

## Text Enhancements
Be aware of the whitespaces.

``` /// ``` in text will be translated into ``` <br><br> ```.

``` // ``` in text will be replaced with ``` <br> ```in display.

``` /s``` in text will be replaced with ``` <br><small><small><small>`` in display.

``` /t``` in text will be replaced with ``` &nbsp;&nbsp;``` in display.

## Installation

Live Display can be lunched from the local filesystem. Just get the file index.html and the folder css into the same directory (e.g. by cloning the repository) and open index.html in the browser. 
